package sg.ncs.product.detectsocialdistancingapp.web.web.controller;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class TestVO {

    @ApiModelProperty("name")
    private String name;
}
